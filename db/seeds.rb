# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#Student.create(name: 'Eki Rivaldi', username: 'Lykoi', age: 17, kelas: 'RPL XI-3', address: 'cigombong bogor', city: 'Bogor', nis: '11800359')
#Student.create(name: 'Iqbal', username: 'iqbal', age: 17, kelas: 'RPL XI-3', address: 'cigombong bogor', city: 'Bogor', nis: '11800359')

# Exam.create(title: 'UH 1', mapel: 'Matematika', duration: '17 jam', nilai: 90.5, keaktifan: 'aktif', level: 'Hard', student_id: '11800789')
# Exam.create(title: 'UH 2', mapel: 'Bahasa Inggris', duration: '2 jam', nilai: 80.5, keaktifan: 'tidak aktif', level: 'Hard', student_id: '11800999')
# Exam.create(title: 'UH 1', mapel: 'Bahasa Inggris', duration: '2 jam', nilai: 80.5, keaktifan: 'tidak aktif', level: 'Hard', student_id: '11800999')
