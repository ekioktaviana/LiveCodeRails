class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :id_transaksi
      t.string :status
      t.string :upload

      t.timestamps
    end
  end
end
