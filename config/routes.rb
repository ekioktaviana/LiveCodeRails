Rails.application.routes.draw do
  
  # get 'payments/edit'
  # get 'payments/index'
  # get 'payments/show'
  # get 'payments/delete'
  resources :students
  resources :exams
  resources :teachers
  resources :reports
  resources :payments

  #get 'reports/edit'
  #get 'reports/index'
  #get 'reports/new'
  #get 'reports/show'
  #get 'exams/edit'
  # get 'exams/index'
  # get 'exams/new'
  # get 'exams/show'
  #get 'teachers/edit'
  #get 'teachers/index'
  #get 'teachers/new'
  #get 'teachers/show'
  #get 'students/edit'
  #get 'students/index'
  #get 'students/new'
  #get 'students/show'
  #get 'books/edit'
 # get 'books/index'
  #get 'books/new'
  #get 'books/show'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
