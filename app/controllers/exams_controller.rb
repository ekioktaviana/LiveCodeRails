class ExamsController < ApplicationController
  def new #untuk menampilkan form data baru
    @exam = Exam.new
    # @exam = Exam.new(title: ' ')
    # @exam = Exam.new(page: nil)
    
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect -> cara mengecek apa isi params
    #title = params[:exam][:title] ->caea mengecek balikan params
    exam = Exam.new(resource_params)
    exam.save
    flash[:notice] = 'Exam has been created'
    #puts exam.errors.messages
    redirect_to exams_path
  end

  def edit # menampilkan data yang sudah disimpian di edit
    @exam = Exam.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @exam = Exam.find(params[:id])
    @exam.update(resource_params)
    flash[:notice] = 'Exam has been updated'
    redirect_to exam_path(@exam)
  end

  def destroy #untuk menghapus data
    @exam = Exam.find(params[:id])
    @exam.destroy
    flash[:notice] = 'Exam has been deleted'
    redirect_to exams_path
  end

  def index #menampilkan seluruh data yang ada di database
    @exams = Exam.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @exam = Exam.find(id) #-> id ini ngambil variabel diatas
    #render plain: id
    #render plain: @exam.title
  end

  private

  def resource_params
    params.require(:exam).permit(:title, :mapel, :duration, :nilai, :keaktifan, :level, :student_id)
  end
end
