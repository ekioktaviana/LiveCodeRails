class StudentsController < ApplicationController
    def new #untuk menampilkan form data baru
      @student = Student.new
      # @student = Student.new(title: ' ')
      # @student = Student.new(page: nil)
      
    end
  
    def create #untuk memproses data baru yang dimasukan di form new
      #render plain: params.inspect -> cara mengecek apa isi params
      #title = params[:student][:title] ->caea mengecek balikan params
      student = Student.new(resource_params)
      student.save
      flash[:notice] = 'Student has been created'
      #puts student.errors.messages
      redirect_to students_path
    end
  
    def edit # menampilkan data yang sudah disimpian di edit
      @student = Student.find(params[:id])
    end
  
    def update #melakukan proses ketika user mengedit data
      @student = Student.find(params[:id])
      @student.update(resource_params)
      flash[:notice] = 'Student has been updated'
      redirect_to student_path(@student)
    end
  
    def destroy #untuk menghapus data
      @student = Student.find(params[:id])
      @student.destroy
      flash[:notice] = 'Student has been deleted'
      redirect_to students_path
    end
  
    def index #menampilkan seluruh data yang ada di database
      @students = Student.all
    end
  
    def show #menampilkan sebuah data secara detail
      id = params[:id]
      @student = Student.find(id) #-> id ini ngambil variabel diatas
      #render plain: id
      #render plain: @student.title
    end
  
    private
  
    def resource_params
      params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nis)
    end
end
  


