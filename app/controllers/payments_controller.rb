class PaymentsController < ApplicationController
  def new #untuk menampilkan form data baru
    @payment = Payment.new
    # @payment = Payment.new(title: ' ')
    # @payment = Payment.new(page: nil)
    
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect -> cara mengecek apa isi params
    #title = params[:payment][:title] ->caea mengecek balikan params
    payment = Payment.new(resource_params)
    payment.save
    flash[:notice] = 'Payment has been created'
    #puts payment.errors.messages
    redirect_to payments_path
  end

  def edit # menampilkan data yang sudah disimpian di edit
    @payment = Payment.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @payment = Payment.find(params[:id])
    @payment.update(resource_params)
    flash[:notice] = 'Payment has been updated'
    redirect_to payment_path(@payment)
  end

  def destroy #untuk menghapus data
    @payment = Payment.find(params[:id])
    @payment.destroy
    flash[:notice] = 'Payment has been deleted'
    redirect_to payments_path
  end

  def index #menampilkan seluruh data yang ada di database
    @payments = Payment.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @payment = Payment.find(id) #-> id ini ngambil variabel diatas
    #render plain: id
    #render plain: @payment.title
  end

  private

  def resource_params
    params.require(:payment).permit(:id_transaksi, :status, :upload)
  end
end
