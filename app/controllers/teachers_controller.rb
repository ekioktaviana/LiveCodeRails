class TeachersController < ApplicationController
  def new #untuk menampilkan form data baru
    @teacher = Teacher.new
    # @teacher = Teacher.new(title: ' ')
    # @teacher = Teacher.new(page: nil)
    
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect -> cara mengecek apa isi params
    #title = params[:teacher][:title] ->caea mengecek balikan params
    teacher = Teacher.new(resource_params)
    teacher.save
    flash[:notice] = 'Teacher has been created'
    #puts teacher.errors.messages
    redirect_to teachers_path
  end

  def edit # menampilkan data yang sudah disimpian di edit
    @teacher = Teacher.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @teacher = Teacher.find(params[:id])
    @teacher.update(resource_params)
    flash[:notice] = 'Teacher has been updated'
    redirect_to teacher_path(@teacher)
  end

  def destroy #untuk menghapus data
    @teacher = Teacher.find(params[:id])
    @teacher.destroy
    flash[:notice] = 'Teacher has been deleted'
    redirect_to teachers_path
  end

  def index #menampilkan seluruh data yang ada di database
    @teachers = Teacher.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @teacher = Teacher.find(id) #-> id ini ngambil variabel diatas
    #render plain: id
    #render plain: @teacher.title
  end

  private

  def resource_params
    params.require(:teacher).permit(:nik, :name, :age, :kelas, :mapel)
  end
end
