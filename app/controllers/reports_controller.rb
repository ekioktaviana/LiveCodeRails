class ReportsController < ApplicationController
  def new #untuk menampilkan form data baru
    @report = Report.new
    # @report = Report.new(title: ' ')
    # @report = Report.new(page: nil)
    
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect -> cara mengecek apa isi params
    #title = params[:report][:title] ->caea mengecek balikan params
    report = Report.new(resource_params)
    report.save
    flash[:notice] = 'Report has been created'
    #puts report.errors.messages
    redirect_to reports_path
  end

  def edit # menampilkan data yang sudah disimpian di edit
    @report = Report.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @report = Report.find(params[:id])
    @report.update(resource_params)
    flash[:notice] = 'Report has been updated'
    redirect_to report_path(@report)
  end

  def destroy #untuk menghapus data
    @report = Report.find(params[:id])
    @report.destroy
    flash[:notice] = 'Report has been deleted'
    redirect_to reports_path
  end

  def index #menampilkan seluruh data yang ada di database
    @reports = Report.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @report = Report.find(id) #-> id ini ngambil variabel diatas
    #render plain: id
    #render plain: @report.title
  end

  private

  def resource_params
    params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :date)
  end
end
